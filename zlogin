# /etc/zsh/zlogin ou ~/.zlogin
# Fichier de configuration de zsh, lu au lancement des shells de login
# Formation Debian GNU/Linux par Alexis de Lattre
# http://formation-debian.via.ecp.fr/

# Ce fichier contient les commandes qui s'exécutent quand l'utilisateur
# ouvre une console

# Afficher des informations sur le systèmes:
#echo "\n#### Info uname ####"
#uname -a

# Affiche l'uptime de la machine
#echo "\n#### uptime ####"
#uptime

# If a network connection is available (check function def in zshrc)
#if [$( is_network )] && [ $(command -v cowsay) ]; then
  # Affiche un dessin (cf fonction définie dans zshrc)
bonjour
#fi

# Accepte les messages d'autres utilisateurs
mesg y

# Affichage le résultat de la commande 'mount' en colonne
#mount | column -t

# Pour les ordinateurs avec un pavé numérique...
# Active le pavé numérique quand on se loggue en console
#case "`tty`" in /dev/tty[1-6]*)
#    setleds +num
#esac

# List tmux sessions if tmux is available
if [ $(command -v tmux) ]; then
	TMUX_SESSION=$(tmux ls 2> /dev/null|grep -- window)
	# If tmux session available and if not already inside a tmux
	if [ ${TMUX_SESSION} ] && [ -z "${TMUX}" ]; then
		printf '%b' "${MAGENTA}++++++++++++++++++++++++ ${WHITEB}Tmux session${RESET} ${MAGENTA}:++++++++++++++++++++++++${RESET}\n"
		tmux ls
	fi
fi

# ssh-agent with fix socket path
## See load-ssh-agent function in ~/.zsh/zshrc
load-ssh-agent

# Recupere un caractere unique
getc ()
{
	stty raw -echo
	tmp=`dd bs=1 count=1 2>/dev/null`
	eval $1='$tmp'
	stty cooked
}

## Autorun an X session with some restrictions for Debian {{{
## Set a ~/.noxorg file to work in tty only
# If non-root session
if [ -f /etc/debian_version ] && [ ${USER} != "root" ] && [ $(command -v startx) ]; then
	# If it's a VC console and
	# no X session already launch and
	# no ~/.noxorg file exist
	if [ -z "${SSH_TTY}" ] &&
	   [ ! -f "${HOME}/.noxorg" ] &&
	   ! pgrep --full -- "/usr/lib/xorg/Xorg|/usr/lib/Xorg" ; then
		echo "Aucune session X11 détectée, voulez vous en lancer une ? [O|n]"
		getc start_x
		if [ `echo "$start_x" | grep "n"` ];then
			echo "X11 ne sera pas lancé."
		else
			`startx -- :1 vt1`
		fi
	fi
fi
## }}}
## Autorun an X session with some restrictions for Archlinux {{{
## Set a ~/.noxorg file to work in tty only
# If non-root session
if [ -f /etc/arch-release ] && [ ${USER} != "root" ] && [ $(command -v startx) ]; then
	# If it's a VC console and
	# no X session already launch and
	# no ~/.noxorg file exist
	if [ -z "${SSH_TTY}" ] &&
	   [ ! -f "${HOME}/.noxorg" ] &&
	   ! pgrep --full -- "/usr/lib/xorg/Xorg|/usr/lib/Xorg" ; then
		echo "No X11 session detected, starting a new one…"
		`startx -- :1 vt1`
	else
		#echo "Not in a VC console, ~/.noxorg file exists or Xorg already running…"
	fi
fi
## }}}
