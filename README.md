# ZSH Configuration

#### Table of contents
1 [Installation - How-to install] (#installation)

## Installation
Clone the repository (with https in this example):

```bash
cd ~/repo
git clone https://git.101010.fr/101010/zsh_config.git
```

To use those configuration files for the system:
```bash
sudo ln -s /home/$USER/repo/101010_zsh /etc/zsh
```


Or for single user:
```bash
ln -s ~/.zlogin ~/repo/101010_zsh/zlogin
ln -s ~/.zlogout ~/repo/101010_zsh/zlogout
ln -s ~/.zprofile ~/repo/101010_zsh/zprofile
ln -s ~/.zshenv ~/repo/101010_zsh/zshenv
ln -s ~/.zshrc ~/repo/101010_zsh/zshrc
```
