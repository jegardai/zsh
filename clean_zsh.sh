#!/bin/sh

# Liste des fichiers à supprimer
## Tous les fichiers de configuration ZSH du système
## Le fichier de configuration et le dossier personnel de ZSH pour l'utilisateur root
## Le fichier de configuration et le dossier personnel de ZSH du dossier courant
## L'archive contenant l'ensemble de la configuration de ZSH
file_list="/etc/zsh/zshrc /etc/zsh/zshenv /etc/zsh/zprofile /etc/zsh/zlogout /etc/zsh/zlogin /root/.zshrc /root/.zsh/ .zshrc .zsh/ zsh_last.tar.gz"

# Recupere un caractere unique
getc ()
{
  stty raw -echo
  tmp=`dd bs=1 count=1 2>/dev/null`
  eval $1='$tmp'
  stty cooked
}

echo "$file_list will be deleted"
echo "Confirm it (y/n):"
getc confirm_delete
if [ `echo "$confirm_delete" | grep "y"` ];then
  echo "Tadadadaaammm"
  #rm -rf $file_list
else
  echo "Good choice"
fi


