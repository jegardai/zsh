# /etc/zsh/zshenv ou ~/.zshenv
# Fichier de configuration de zsh, lu au lancement de tout shell Zsh
# Formation Debian GNU/Linux par Alexis de Lattre
# http://formation-debian.via.ecp.fr/

# Colors definition {{{
export BLACK='\033[30;40m'
export RED='\033[0;31m'
export REDB='\033[1;31m'
export GREEN='\033[1;32m'
export YELLOW='\033[1;33m'
export BLUE='\033[34;40m'
export MAGENTA='\033[0;35m'
export MAGENTAB='\033[1;35m'
export CYAN='\033[36;40m'
export WHITE='\033[0;37m'
export WHITEB='\033[1;37m'
export RESET='\033[0m'
# }}}

# Ensure to use ZSH for this session
export SHELL='/bin/zsh'

# PATH
export PATH="/usr/local/bin:/usr/local/sbin:/bin:/usr/bin:/usr/sbin:/usr/bin/X11:/usr/games:/sbin:$HOME/bin:/opt:/usr/local/games:$HOME/.virtualenvs/bin:$HOME/.local/bin"

# Default soft
export VISUAL=vim
export EDITOR=vim
## web browser {{{
if [ $(command -v qutebrowser) ]; then
  export BROWSER=qutebrowser
elif [ $(command -v firefox) ]; then
  export BROWSER=firefox
fi
## }}}
## Terminal emulator {{{
if [ $(command -v alacritty) ]; then
  export TERM_EMULATOR=alacritty
fi
## }}}
## Pager (man,…) {{{
if [ $(command -v vimmanpager) ]; then
  export PAGER=vimmanpager
else
  export PAGER=less
fi
alias zless=$PAGER
## }}}
## Git pager {{{
if [ $(command -v bat) ]; then
	export GIT_PAGER=bat
else
	export GIT_PAGER="less -F -X"
fi
## }}}

# Colors definition for less {{{
export LESS_TERMCAP_mb=$'\E[01;31m'    # début de blink
export LESS_TERMCAP_md=$'\E[01;31m'    # début de gras
export LESS_TERMCAP_me=$'\E[0m'        # fin
export LESS_TERMCAP_so=$'\E[01;44;33m' # début de la ligne d`état
export LESS_TERMCAP_se=$'\E[0m'        # fin
export LESS_TERMCAP_us=$'\E[01;32m'    # début de souligné
export LESS_TERMCAP_ue=$'\E[0m'        # fin
# }}}

# Limitation de la taille de la pile de dossier:
DIRSTACKSIZE=10
export DIRSTACKSIZE

# Default PDF viewer {{{
if [ $(command -v zathura) ]; then
	export PDF_VIEWER="zathura"
elif [ $(command -v evince) ]; then
	export PDF_VIEWER="evince"
fi
# }}}

# Permissions for new files
## rw-r--r-- − files
## rwxr-xr-x − directories
umask 022

# Proxy HTTP / FTP sans mot de passe {{{
#export http_proxy="http://localhost:8118"
#export ftp_proxy="ftp://proxy.exemple.org:8080"
# }}}
# Proxy HTTP / FTP avec mot de passe {{{
#export http_proxy="http://login:password@proxy.exemple.org:8080"
#export ftp_proxy="ftp://login:password@proxy.exemple.org:8080"
# }}}
# Ne pas passer par le proxy pour les domaines locaux {{{
#export no_proxy="exemple.org"
# }}}

# Lock unused session after 10 minutes {{{
#export TMOUT=600
# }}}
# Close unused root session after 30 minutes {{{
if [ ${USER} = "root" ]; then
  export TMOUT=1800
fi
# }}}

# Tmux {{{
if [ $(command -v tmux) ]; then
  export TMUX_TMPDIR=~/.tmux/socks/
  [ ! -d "${TMUX_TMPDIR}" ] && mkdir -p -- "${TMUX_TMPDIR}" || chmod 0700 -- "${TMUX_TMPDIR}"
fi
# }}}

# SSH {{{
## Specify ssh-agent socket path
SSH_AGENT_SOCK="${HOME}/.ssh/socks/ssh_auth"
SSH_AGENT_ENV="${HOME}/.ssh/socks/ssh_agent_env"
## ssh-keys path
SSH_ED25519_KEY="${HOME}/.ssh/id_ed25519"
SSH_RSA_KEY="${HOME}/.ssh/id_rsa"
# }}}
# GPG {{{
GPG_PRIV_KEY="${HOME}/.gnupg/secring.gpg"
GPG_TEST_FILE="${HOME}/.password-store/test"
# }}}

# Bemoji {{{
# Emoji picker https://github.com/marty-oehme/bemoji
if [ $(command -v bemoji) ]; then
	## Set rofi options
	export BEMOJI_PICKER_CMD="rofi -theme solarized_alternate -location 2 -p 🧐 -i -dmenu"
fi
# }}}
