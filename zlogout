# /etc/zsh/zlogout ou ~/.zlogout
# Fichier de configuration de zsh, lu à la fermeture des shells de login
# Formation Debian GNU/Linux par Alexis de Lattre
# http://formation-debian.via.ecp.fr/

# Ce fichier contient les commandes qui s'exécutent quand l'utilisateur
# ferme une console

# Delete sensitive files can contains unwanted clear passwords
rm -f ~/.config/evince/print-settings ~/.config/eog/eog-print-settings.ini

# Clean the screen
clear
